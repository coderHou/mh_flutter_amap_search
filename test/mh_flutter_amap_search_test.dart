import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mh_flutter_amap_search/mh_flutter_amap_search.dart';

void main() {
  const MethodChannel channel = MethodChannel('mh_flutter_amap_search');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await MhFlutterAmapSearch.platformVersion, '42');
  });
}
