import AMapSearchKit

extension AMapPOI {
    public func toJson() -> Dictionary<String, Any>{
        var json:[String:Any] = [:]
        if uid != nil {
            json["poiId"] = uid
        }
        json["title"] = name
        json["address"] = address
        json["latitude"] = NSNumber.init(value:Float(location.latitude))
        json["longitude"] = NSNumber.init(value:Float(location.longitude))
        json["city"] = city
        json["province"] = province
        json["cityCode"] = String(citycode)
        json["provinceCode"] = String(pcode)
        json["businessArea"] = businessArea
        json["adCode"] = String(adcode)
        json["adName"] = district
        json["distance"] = NSNumber(value: Int(distance))
        json["tel"] = tel
        
        return json
    }
}

extension AMapGeocode {
    public func toJson() -> Dictionary<String, Any>{
        var json:[String:Any] = [:]
        json["formattedAddress"] = formattedAddress
        json["neighborhood"] = neighborhood
        json["latitude"] = NSNumber.init(value:Float(location.latitude))
        json["longitude"] = NSNumber.init(value:Float(location.longitude))
        json["city"] = city
        json["province"] = province
        json["cityCode"] = String(citycode)
        json["adCode"] = String(adcode)
        json["district"] = district
        json["level"] = String(level)
        json["township"] = township
        
        return json
    }
}

extension AMapTip {
    public func toJson() -> Dictionary<String, Any>{
        var json:[String:Any] = [:]
        json["uid"] = String(uid)
        json["name"] = name
        json["latitude"] = NSNumber.init(value:Float(location.latitude))
        json["longitude"] = NSNumber.init(value:Float(location.longitude))
        json["address"] = address
        json["adCode"] = String(adcode)
        json["district"] = district
        json["typecode"] = String(typecode)
        
        return json
    }
}
extension AMapAddressComponent{
    public func toJson() -> Dictionary<String, Any>{
        var json:[String:Any] = [:]
        json["neighborhood"] = neighborhood
        json["city"] = city
        json["province"] = province
        json["cityCode"] = String(citycode)
        json["adCode"] = String(adcode)
        json["district"] = district
        json["township"] = township
        json["townCode"] = String(towncode)
        return json
    }
}

extension AMapReGeocode {
    public func toJson() -> Dictionary<String, Any>{
        var json:[String:Any] = [:]
        json["formattedAddress"] = formattedAddress
        json["addressComponent"] = addressComponent.toJson()
        let poiList = pois?.map { poi in
            poi.toJson()
        }
        json["pois"] = poiList ?? []
        
        return json
    }
}

extension AMapDistanceResult {
    public func toJson() -> Dictionary<String, Any>{
        var json:[String:Any] = [:]
        json["originID"] = NSNumber(value: originID)
        json["destID"] = NSNumber(value:destID)
        json["distance"] = NSNumber(value:distance)
        json["duration"] = NSNumber(value:duration)
        json["info"] = info
        json["code"] = NSNumber(value:code)
        return json
    }
}
