#import "MhFlutterAmapSearchPlugin.h"
#if __has_include(<mh_flutter_amap_search/mh_flutter_amap_search-Swift.h>)
#import <mh_flutter_amap_search/mh_flutter_amap_search-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "mh_flutter_amap_search-Swift.h"
#endif

@implementation MhFlutterAmapSearchPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMhFlutterAmapSearchPlugin registerWithRegistrar:registrar];
}
@end
