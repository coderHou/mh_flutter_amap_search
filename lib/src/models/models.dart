part of MhFlutterAmapSearch;

/// 兴趣点 model
class Poi {
  Poi({
    required this.address,
    required this.title,
    required this.latLng,
    required this.cityName,
    required this.cityCode,
    required this.provinceName,
    required this.provinceCode,
    required this.tel,
    required this.poiId,
    required this.businessArea,
    required this.distance,
    required this.adName,
    required this.adCode,
  });

  static  Poi fromJson(Map json){
    // if(json.isEmpty) return null;
    return Poi(
      address:json["address"] ?? "",
      title:json["title"] ?? "",
      latLng:LatLng(json["latitude"],json["longitude"]),
      cityName:json["cityName"] ?? "",
      cityCode:json["cityCode"] ?? "",
      provinceName:json["province"] ?? "",
      provinceCode:json["provinceCode"] ?? "",
      tel:json["tel"] ?? "",
      poiId:json["poiId"] ?? "",
      businessArea:json["businessArea"] ?? "",
      distance:json["distance"] ?? 0,
      adName:json["adName"] ?? "",
      adCode:json["adCode"] ?? "",
    );
  }

  /// 地址
  String address;

  ///标题
  String title;

  /// 经纬度
  LatLng latLng;

  /// 城市名
  String cityName;

  /// 城市编码
  String cityCode;

  /// 省份名称
  String provinceName;

  /// 省份编码
  String provinceCode;

  /// 电话
  String tel;

  /// 兴趣点id
  String poiId;

  /// 商业区
  String businessArea;

  /// 距离
  int distance;

  /// 行政区划名称
  String adName;

  /// 行政区划编号
  String adCode;


  @override
  String toString() {
    return 'Poi{address: $address, title: $title, latLng: $latLng, cityName: $cityName, cityCode: $cityCode, provinceName: $provinceName, provinceCode: $provinceCode, tel: $tel, poiId: $poiId, businessArea: $businessArea, distance: $distance, adName: $adName, adCode: $adCode}';
  }
}

/// 输入提示 model
class InputTip {
  InputTip({
    required this.name,
    required this.poiId,
    required this.address,
    required this.district,
    required this.adCode,
    required this.coordinate,
  });
  static InputTip fromJson(Map<String, dynamic> json){
    return InputTip(
        name: json["name"] ?? "",
        poiId: json["uid"] ?? "",
        address: json["address"] ?? "",
        district: json["district"] ?? "",
        adCode: json["adCode"] ?? "",
        coordinate: LatLng(json["latitude"], json["longitude"])
    );
  }
  /// 提示名称
  final String name;

  /// 兴趣点id
  final String poiId;

  /// 地址
  final String address;

  /// 区域
  final String district;
  /// 区域
  final String adCode;

  /// 经纬度
  final LatLng coordinate;

  @override
  String toString() {
    return 'InputTip{name: $name, poiId: $poiId, address: $address, district: $district, coordinate: $coordinate}';
  }
}
/// 地理编码 model
class Geocode {
  Geocode({
   required this.latLng,
    required this.formattedAddress,
    required this.adCode,
    required this.district,
    required this.cityCode,
    required this.cityName,
    required this.provinceName,
    required this.level,
    required this.township
  });

  static Geocode fromJson(Map<String, dynamic> json){
    return Geocode(
        formattedAddress: json["formattedAddress"] ?? "",
        adCode: json["adCode"] ?? "",
        district: json["district"] ?? "",
        cityName: json["city"] ?? "",
        cityCode: json["cityCode"] ?? "",
        provinceName: json["province"] ?? "",
        level: json["level"] ?? "",
        township: json["township"] ?? "",
        latLng: LatLng(json["latitude"], json["longitude"])
    );
  }
  /// 经纬度
  final LatLng latLng;
  /// 地址
  String formattedAddress;

  ///code
  String adCode;

  ///区
  String district;

  /// 城市名
  String cityName;

  /// 城市编码
  String cityCode;

  /// 省份名称
  String provinceName;

  ///
  String level;

  ///
  String township;

  @override
  String toString() {
    return 'Geocode{latLng: $latLng}';
  }
}

/// 逆地理编码 model
class ReGeocode {
  ReGeocode({
    required this.provinceName,
    required this.cityName,
    required this.cityCode,
    required this.adCode,
    required this.districtName,
    required this.townCode,
    required this.township,
    required this.neighborhood,
    // this.building,
    // this.country,
    required this.formatAddress,
    // this.roads,
    // this.aoiList,
    required this.poiList,
  });

  /// 省份名称
  final String provinceName;

  /// 城市名称
  final String cityName;

  /// 城市代码
  final String cityCode;

  /// 邮政
  final String adCode;

  /// 区域名称
  final String districtName;

  /// 乡镇编码
  final String townCode;

  /// 乡镇名称
  final String township;

  /// 社区名称
  final String neighborhood;

  // /// 建筑物
  // final String building;
  //
  // /// 国家
  // final String country;

  /// 地址全称
  final String formatAddress;

  // /// 道路列表
  // final List<Road> roads;
  //
  // /// 兴趣区域列表
  // final List<Aoi> aoiList;

  /// 兴趣点列表
  final List<Poi> poiList;

  static ReGeocode fromJson(Map<String,dynamic> json){
    var addressComponent = json["addressComponent"] as Map<String,dynamic>;
    var pois = json["pois"] as List;
    return ReGeocode(
      formatAddress: json["formattedAddress"] ?? "",
      neighborhood: addressComponent["neighborhood"] ?? "",
      cityCode: addressComponent["cityCode"] ?? "",
      cityName: addressComponent["city"] ?? "",
      provinceName: addressComponent["province"] ?? "",
      adCode: addressComponent["adCode"] ?? "",
      districtName: addressComponent["district"] ?? "",
      township: addressComponent["township"] ?? "",
      townCode: addressComponent["townCode"] ?? "",
      poiList: pois.map((e) => Poi.fromJson(e)).toList()
    );
  }
  @override
  String toString() {
    return 'ReGeocode{provinceName: $provinceName, cityName: $cityName, cityCode: $cityCode, adCode: $adCode, districtName: $districtName, townCode: $townCode, township: $township, neighborhood: $neighborhood,  formatAddress: $formatAddress,  poiList: $poiList}';//building: $building, country: $country,roads: $roads, aoiList: $aoiList,
  }
}

// /// 道路
// class Road {
//   Road({
//     this.id,
//     this.name,
//     this.distance,
//     this.direction,
//     this.coordinate,
//   });
//
//   String id;
//   String name;
//   double distance;
//   String direction;
//   LatLng coordinate;
//
//   @override
//   String toString() {
//     return 'Road{id: $id, name: $name, distance: $distance, direction: $direction, coordinate: $coordinate}';
//   }
// }
//
// class Aoi {
//   Aoi({
//     this.adCode,
//     this.area,
//     this.id,
//     this.name,
//     this.centerPoint,
//   });
//
//   /// 邮政编码
//   final String adCode;
//
//   /// 覆盖面积 单位平方米
//   final double area;
//
//   /// 唯一标识
//   final String id;
//
//   /// 名称
//   final String name;
//
//   /// 中心点坐标
//   final LatLng centerPoint;
//
//   @override
//   String toString() {
//     return 'Aoi{adCode: $adCode, area: $area, id: $id, name: $name, centerPoint: $centerPoint}';
//   }
// }