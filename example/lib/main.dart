import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:mh_flutter_amap_search/mh_flutter_amap_search.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  var poiList = <Poi>[];
  bool isLoading = false;
  final _textFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
    MhFlutterAmapSearch.setApiKey("cb56011e3eac9b5993b3d6297ddaaa12", "6c43a85e5cba91cf6139f3994e01de5a");
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await MhFlutterAmapSearch.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              Text('Running on: $_platformVersion\n'),
              Row(
                mainAxisSize:MainAxisSize.min,
                children: [
                  Expanded(
                    child: TextField(
                      controller: _textFieldController,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  MaterialButton(
                    child: Text('search_key'),
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var result = await MhFlutterAmapSearch.searchKeyword(_textFieldController.text);
                        setState(() {
                          isLoading = false;
                          poiList = result;
                        });
                      }),
                ],
              ),
              Wrap(
                children: [
                  MaterialButton(
                      child: Text('search_around'),
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var result = await MhFlutterAmapSearch.searchAround(poiList.first.latLng);
                        setState(() {
                          isLoading = false;
                          poiList = result;
                        });
                      }),
                  MaterialButton(
                      child: Text('search_geo'),
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var result = await MhFlutterAmapSearch.searchGeocode(poiList.last.address);
                        setState(() {
                          isLoading = false;
                          // poiList = result;
                        });
                      }),
                  MaterialButton(
                      child: Text('search_reGeo'),
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var result = await MhFlutterAmapSearch.searchReGeocode(poiList.last.latLng);
                        setState(() {
                          isLoading = false;
                          // poiList = result;
                        });
                      }),
                  MaterialButton(
                      child: Text('search_id'),
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var result = await MhFlutterAmapSearch.searchPoiId(poiList.last.poiId);
                        setState(() {
                          isLoading = false;
                          // poiList = result;
                        });
                      }),
                  MaterialButton(
                      child: Text('search_distance'),
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var result = await MhFlutterAmapSearch.searchDistance(origins:[poiList.first.latLng],destination: poiList.last.latLng);
                        setState(() {
                          isLoading = false;
                          // poiList = result;
                        });
                      }),
                  MaterialButton(
                      child: Text('search_tips'),
                      onPressed: () async {
                        setState(() {
                          isLoading = true;
                        });
                        var result = await MhFlutterAmapSearch.fetchInputTips('c');
                        setState(() {
                          isLoading = false;
                          // poiList = result;
                        });
                      }),
                ],
              ),
              // MaterialButton(
              //     child: Text('search天河城'),
              //     onPressed: () async {
              //       debugPrint('---beginbedin天河城---');
              //       try{
              //         debugPrint('---trytry天河城---');
              //         var result = await MhFlutterAmapSearch.searchKeyword("天河城");
              //         setState(() {
              //           poiList = result;
              //         });
              //       }catch(e){
              //         debugPrint('天河城error${e.toString()}');
              //       }
              //       debugPrint('---天河城wanwnananw---');
              //       //     .then((value){
              //       //   debugPrint('--result ${value.toString()}---');
              //       // },onError: (e){
              //       //   debugPrint('---err $e---');
              //       // });
              //     }),
              isLoading? CupertinoActivityIndicator(radius: 14) :  Expanded(
                  child: ListView.builder(
                      itemCount: poiList.length,
                      itemBuilder: (context,index){
                        var poi = poiList[index];
                        return ListTile(
                          title: Text(poi.title),
                          subtitle: Text(poi.address),
                        );
                      }))
            ],
          ),
        ),
      ),
    );
  }
}
